import { test } from 'qunit';
import moduleForAcceptance from 'try-ember-cli-typescript/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | index');

test('visiting /index', function(assert) {
  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/');
    assert.equal(find('h1').text().trim(), 'hello', '.ts helper value displayed');
  });
});
