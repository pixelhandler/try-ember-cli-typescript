import Ember from 'ember';
import helloTypescript from '../utils/hello-typescript';

export default Ember.Controller.extend({
  greeting: helloTypescript()
});
